<?php
namespace app\common\lib;

class IAuth {

    /**t
     * todo 设置登录的token  - 唯一性的
     * @param string $phone
     * @return string
     */
    public  static function setWxLoginToken($userPhone = '') {
        $str = md5(uniqid(md5(microtime(true)), true));
        $str = sha1($str.$userPhone);
        return $str;
    }


    /**todo 发起HTTP请求(身份证与姓名匹配)
     * @param $url
     * @param string $type
     * @param string $res
     * @param string $arr
     * @return mixed|string
     */
    public static function http_curl($url,$type='get',$res='json',$arr=''){
        //1.初始化curl
        $ch = curl_init();
        //2.设置curl的参数
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 信任任何证书
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); // 检查证书中是否设置域名
        if($type == 'post'){
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $arr);
        }
        //3.采集
        $output = curl_exec($ch);
        //4.关闭
        if($res == 'json'){
            if(curl_errno($ch)){
                return curl_error($ch);
            }else{
                return json_decode($output,true);
            }
        }
        curl_close($output);
    }
}
