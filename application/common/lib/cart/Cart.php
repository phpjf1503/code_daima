<?php
namespace app\common\lib;
use app\api\model\Cart as shopCart;
use app\api\model\Goods;
/**todo 购物车类
 * Class Cart
 * @package app\common\lib
 */
class Cart {

    /**静态方法
     * todo 新增商品（注意: 判断是否已经存在如果存在则只增加数量1 判断库存是否已经超过了)
     * 如果不存在则直接添加一条记录
     * @param $user_id  //客户id
     * @param $good_id //商品id
     * @param $good_num //商品数量默认为1 不需要传
     *
     */
    public  static function addOneGood($user_id = 0,$good_id = 0,$good_num = 1){
        $cartModel = (new shopCart());
        $goodIsExist = $cartModel->checkGoodIsExist($user_id,$good_id);
        if(!$goodIsExist){
            $addData = [
                "user_id" => $user_id,
                "good_id" => $good_id,
                "good_num" => $good_num,//默认为1
                "create_time" => time(),
            ];
            try{
                $addCartResult = $cartModel->addUpdateData($addData);
            }catch (\Exception $e){
                return APIresponse(0,$e->getMessage(),[]);
            }
            return APIresponse(200,"添加购物车成功~",[]);
        }else{
            $cartData = $cartModel->getCartGoodData($user_id,$good_id);
            $number = $cartData["good_num"];//目前此商品的数量多少
            $cartID = $cartData["id"];//购物车id
            $number = ($number + 1);
            $checkResult = self::checkStock($number,$good_id);
            if(!$checkResult){
                return APIresponse(0,"超过了库存~",[]);
            }else{
                //修改购物车数据
                $updateData = [
                    "id" => $cartID,
                    "good_num" => $number,
                    "update_time" => time(),
                ];
                try{
                    $updateCartResult = $cartModel->saveUpdateData($updateData);
                }catch (\Exception $e){
                    return APIresponse(0,$e->getMessage(),[]);
                }
                return APIresponse(200,"更新购物车商品数量成功~",[]);
            }
        }
    }
    /**
     * todo 判断库存是否已经超过了
     */
    public static function checkStock($number = 0,$good_id = 0){
        $goodModel = (new Goods());
        $oneGoodData = $goodModel->getGoodInfoById($good_id);//获取单个商品信息
        $goodStock = $oneGoodData["stock"];//获取此商品的库存是多少
        if($number > $goodStock){
            return false;
        }else{
            return true;
        }
    }

    /**todo 修改购物车中的商品数量
     * 注意: 商品数量不能小于1(0) 商品数量不能大于库存
     * @param int $user_id
     * @param int $good_id
     * @param int $good_num
     */
    public static function editCart($user_id = 0,$good_id = 0,$number = 0){
        $cartModel = (new shopCart());
        if(!$number || $number < 1){
            return APIresponse(0,"商品数据不能小于1~",[]);
        }
        $checkResult = self::checkStock($number,$good_id);
        if(!$checkResult){
            return APIresponse(0,"超过了库存~",[]);
        }else{
            $cartData = $cartModel->getCartGoodData($user_id,$good_id);
            $cartID = $cartData["id"];
            //修改购物车数据
            $updateData = [
                "id" => $cartID,
                "good_num" => $number,
                "update_time" => time(),
            ];
            try{
                $updateCartResult = $cartModel->saveUpdateData($updateData);
            }catch (\Exception $e){
                return APIresponse(0,$e->getMessage(),[]);
            }
            return APIresponse(200,"更新购物车商品数量成功~",[]);
        }
    }
    /**
     * todo 删除购物车中某个商品的记录
     * 1.首先判断其记录是否存在
     */
    public static function deleteCartGood($user_id = 0,$good_id = 0){
        $cartModel = (new shopCart());
        $cartData = $cartModel->getCartGoodData($user_id,$good_id);
        if(!$cartData){
            return APIresponse(0,"此商品不在购物车无法删除~",[]);
        }else{
            $cartID = $cartData["id"];
        }
        try{
            $deleteResult = $cartModel->deleteCartGood($cartID);
        }catch (\Exception $e){
            return APIresponse(0,$e->getMessage(),[]);
        }
        return APIresponse(200,"删除购物车商品成功~");
    }
    /**
     * todo 将多个选中的商品记录删除掉
     */
    public static function deleteCheckedCartGoods($user_id = 0){
        if(!$user_id){
            return false;
        }
        $cartModel = (new shopCart());
        try{
            $deletesResult = $cartModel->deleteCartGoods($user_id);
        }catch (\Exception $e){
            return false;
        }
        return true;
    }
    /**
     * todo 修改选中的状态
     */
    public static function setCartCheckedStatus($user_id = 0,$good_id = 0,$isChecked = 0){
        $cartModel = (new shopCart());
        $cartData = $cartModel->getCartGoodData($user_id,$good_id);
        if(!$cartData){
            return APIresponse(0,"此商品不在购物车~",[]);
        }else{
            $cartID = $cartData["id"];
        }
        try{
            $setResult = $cartModel->setCartCheckedStatus($isChecked,$cartID);
        }catch (\Exception $e){
            return APIresponse(0,$e->getMessage(),[]);
        }
        return APIresponse(200,"修改选中的状态成功~");
    }


}
