<?php
namespace app\common\lib\exception;
use think\Exception;

class ApiException extends Exception {

    public $message = '';
    public $httpCode = 500;
    public $code = 0;
    /**todo 捕获异常设置变量
     * @param string $message 提示信息
     * @param int $httpCode HTTP状态码
     * @param int $code 状态码
     */
    public function __construct($message = '', $httpCode = 0, $code = 0) {
        $this->httpCode = $httpCode;
        $this->message = $message;
        $this->code = $code;
    }
}