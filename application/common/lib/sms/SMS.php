<?php
namespace app\common\lib;
//设置请求头
use think\facade\Config;

header("Content-Type:application/x-www-form-urlencoded;charset=utf-8;");
/**TODO 云片短信接口类
 * Class SMS
 * @package app\common\lib
 */
class SMS{

    /**TODO 批量发送短信的方法
     * @param $mobile   手机号码(接受短信) 多个手机号码 发送多个手机号请以英文逗号分隔，
     * @param $text     文案(必须与云片后台模板匹配)
     * @return mixed
     */
    public static function sendBatchSms($mobile,$text){
        $url = Config::get("admin.sms_send_batch");
        $params = array(
            "apikey" => Config::get("admin.sms_apikey"),
            "mobile" => $mobile,
            "text" => $text,
        );
        $responseData = self::http_curl($url,$params);
        return $responseData;
    }
    /**TODO 发送单条短信的方法
     * @param $mobile  手机号码(接受短信)
     * @param $text     文案(必须与云片后台模板匹配)
     * @return mixed
     */
    public static function sendSingleSms($mobile,$text){
        //单条发送接口请求地址
        $url = Config::get("admin.sms_send_single");
        $params = array(
            "apikey" => Config::get("admin.sms_apikey"),//apikey
            "mobile" => $mobile,
            "text" => $text
        );
        $responseData = self::http_curl($url,$params);
        return $responseData;
    }
    /** TODO CURL请求云片短信接口
     * @param $url
     * @param array $params
     * @return mixed
     */
    public static function http_curl($url,$params = []){
        //1.初始化curl
        $ch = curl_init();
        //2.设置curl的参数
        curl_setopt($ch, CURLOPT_URL, $url);
        //将请求参数进行处理
        $param = "";
        foreach ($params as $paramsK=>&$paramsV){
            $param .= urlencode($paramsK).'='.urlencode($paramsV).'&';
        }
        $param = substr($param,0,strlen($param)-1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
        curl_setopt($ch,CURLOPT_HEADER,0);
        curl_setopt($ch,CURLOPT_POST,1);//设置请求方式为POST
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);//不验证证书 (下同)
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,false);
        //return the transfer as a string
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        //$output contains the output string
        $output = curl_exec($ch);
        //close curl resource to free up system resources
        curl_close($ch);
        //$outputJson = json_decode($output);
        $outputArr = json_decode($output,true);
        return $outputArr;
    }
}
